"""
Detects and trim abnormally long branches using TreeShrink v1.3.2

TreeShrink must be installed and on path
Phyx (pxrr) must be installed and on path

"""


import sys, os, shutil, glob, re
from Bio import Phylo
import pandas as pd


def trim(inDIR,tree_file_ending,q,outDIR,exceptions):

	if os.path.isabs(inDIR) == False: inDIR = os.path.abspath(inDIR)
	if inDIR[-1] != "/": inDIR += "/"
	if os.path.isabs(outDIR) == False: outDIR = os.path.abspath(outDIR)
	if outDIR[-1] != "/": outDIR += "/"
	if outDIR == ".": outDIR = os.getcwd()
	
	
	filecount = 0

	with open(exceptions) as f:
	    taxa_to_not_remove = f.read().splitlines()
	    #print taxa_to_not_remove

	

	for i in os.listdir(inDIR):
		if i.endswith(tree_file_ending):
			original_tre = Phylo.read(inDIR+i, "newick")
			original_tre_tips = []
			for leaf in original_tre.get_terminals(): original_tre_tips.append(leaf.name)
			ori_tip_db = pd.DataFrame(original_tre_tips)
			#print ori_tip_db
			
	    
			newlist = []
			for taxa in taxa_to_not_remove:
				r = re.compile(str(taxa)+".*")
				newlist.append(filter(r.match, original_tre_tips))
				
			flatlist = []
			for sublist in newlist:
				for item in sublist:
					flatlist.append(item)
			print
			
			if len(flatlist) >= 1:
						
	
				os.path.splitext(outDIR+i)[0]
				#print os.path.splitext(outDIR+i)[0]
				filecount += 1
				cmd= ["python  ~/Apps/TreeShrink/run_treeshrink.py","-t", inDIR+i ,"-c","-m per-gene", "-q "+str(q), "-o",outDIR+i+".ts_dir", "-O"+os.path.splitext(outDIR+i)[0], "-x"]
				print ((" ".join(cmd))+(" ")+(','.join(flatlist)))
				os.system((" ".join(cmd))+(" ")+(','.join(flatlist)))
				
			
			else: 

				os.path.splitext(outDIR+i)[0]
				#print os.path.splitext(outDIR+i)[0]
				filecount += 1
				cmd= ["python  ~/Apps/TreeShrink/run_treeshrink.py","-t", inDIR+i ,"-c","-m per-gene", "-q "+str(q), "-o",outDIR+i+".ts_dir", "-O"+os.path.splitext(outDIR+i)[0]]
				print ((" ".join(cmd))+(" ")+(','.join(flatlist)))
				os.system((" ".join(cmd))+(" ")+(','.join(flatlist)))
			
			
			
	

	"""	# keep out	
	#go into each TS folder and change extension from 'tree_file_ending' to ts
	
	for f in os.listdir(outDIR):
		if f.endswith(".ts_dir"):
			for t in os.listdir(outDIR+f):
				if t.endswith(tree_file_ending):
					if f[-1] != "/": f += "/"			
					basename = os.path.splitext(outDIR+f+t)[0]
					os.rename(outDIR+f+t, basename + ".ts")
	"""				
					

	#moves output files to DIR and delete treeshrink individual folders
	for j in os.listdir(outDIR):
		if j.endswith(".ts_dir"):
			source = outDIR+j
			dest = outDIR
			files = os.listdir(source)
			for f in files:
				shutil.move(source+"/"+f, dest)
			shutil.rmtree(outDIR+j)
	
	
	#removes single quotes from tip labels from treeshrink output trees
	for k in os.listdir(outDIR):
		if k.endswith(".mm"):
			with open(outDIR+k, 'r+') as f:
				content = f.read()
				f.seek(0)
				f.truncate()
				f.write(content.replace("'", ""))
			f.close()
	
	#unroot treeshrink ouput trees
	for l in os.listdir(outDIR):
		if l.endswith(".mm"):
			cmd= ["pxrr","-u","-t", outDIR+l,"-o",outDIR+l+".ts"]
			#print (" ".join(cmd))
			os.system(" ".join(cmd))			
			
	
	#delete ts files
	for m in os.listdir(outDIR):
			if m.endswith(".mm"):
    				os.remove(outDIR+m)
        
            
	assert filecount > 0, \
		"No file end with "+tree_file_ending+" found in "+inDIR
			
if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python tree_shrink_wrapper.py inDIR tree_file_ending quantile outDIR exceptions"
		sys.exit(0)

	inDIR,tree_file_ending,q,outDIR,exceptions = sys.argv[1:]
	trim(inDIR,tree_file_ending,q,outDIR,exceptions)
